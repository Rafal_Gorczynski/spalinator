//
//  CarRecord.swift
//  Spalinator
//
//  Created by Rafal Gorczynski on 07.05.2017.
//  Copyright © 2017 Rafal Gorczynski. All rights reserved.
//

import Foundation

class CarRecord {
    let brand: String?
    let model: String?
    let fuelType: String? //change it later
    var milage: Int
    var consumption: Float

    init (brand: String, model: String, fuelType: String, milage: Int, consumption: Float) {
        self.brand = brand
        self.model = model
        self.fuelType = fuelType
        self.milage = milage
        self.consumption = consumption
    }
}
