//
//  MainTableViewCell.swift
//  Spalinator
//
//  Copyright © 2017 Rafal Gorczynski. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var fuelType: UILabel!
    @IBOutlet weak var milage: UILabel!
    @IBOutlet weak var consumption: UILabel!

    override func awakeFromNib() {
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(with model: CarRecord) {
        self.brand.text = model.brand
        self.model.text = model.model
        self.fuelType.text = model.fuelType
        self.milage.text = String(model.milage)
        self.consumption.text = String(model.consumption)

    }

}
